export class CardData {
  cardData: any;
  cardPublicKey: string;
  curve: string;
  firmwareVersion: string;
  health: number;
  isActivated: boolean;
  issuerPublicKey: string;
  manufacturerName: string;
  maxSignatures: number;
  pauseBeforePin2: number;
  settingsMask: any;
  signingMethods: any;
  status: string;
  terminalIsLinked: boolean;
  walletPublicKey: string;
  walletRemainingSignatures: number;
  walletSignedHashes: number;

  keyGenerator() {
    return this.walletPublicKey;
  }
}
