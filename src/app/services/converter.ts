import { FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
export class Converter {
  constructor(private http: HttpClient) {}

  satsToBtc(sats) {
    const result = (sats * 0.00000001).toString();
    return result.substr(0, 10);
  }
  btcToSats(btc) {
    const result = (btc / 0.00000001).toString();
    return result.substr(0, 10);
  }
  convertStringToHex(str) {
    const arr = [];
    for (let i = 0; i < str.length; i++) {
      arr[i] = ('00' + str.charCodeAt(i).toString(16)).slice(-4);
    }
    return '\\u' + arr.join('\\u');
  }
  convertHexToString(str) {
    return unescape(str.replace(/\\/g, '%'));
  }

  //   getPrice() {
  //     return this.http.get('https://blockchain.info/ticker');
  //   }
}
