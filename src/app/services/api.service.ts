import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Location, formatCurrency } from '@angular/common';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http: HttpClient) {}

  getAddressFromKey(key): Observable<any> {
    return this.http.get('https://blockchain.info/q/addrpubkey/' + key);
  }

  getAddressFromKey2(key: string) {
    return this.http
      .get('https://blockchain.info/q/addrpubkey/' + key)
      .pipe(
        map(
          (response: { error: { text: string;  } }) =>
            response.error.text
        )
      );
    // prettier-ignore
    // return this.http.get('https://blockchain.info/q/addrpubkey/' + key).subscribe(
    //   () => {},
    //   (data) => {
    //     if (data.statusText === 'OK') {
    //       return data.error.text;
    //     }
    //   }
    // );// https://blog.fullstacktraining.com/loading-data-before-components-in-angular/
  }

  getPrice(formatted?) {
    // prettier-ignore

    if (!formatted) {
      return this.http
      .get(
        'https://blockchain.info/ticker'
      )
      .subscribe((data: { USD}) => {
        return data.USD.last;
      });

    } else {
      return this.http
      .get(
        'https://blockchain.info/ticker'
      ).subscribe((data: { USD}) => {
        return  formatCurrency(data.USD.last, 'en-US', '$');
      });
    }
  }
  sats2usd(sats) {
    let usd;
    const cat = this.http
      .get('https://blockchain.info/ticker')
      .subscribe((data: { USD }) => {
        const price = data.USD.last;
        const usd = sats * 0.00000001;
        const total = usd * price;
        const cat = total.toFixed(2);
        return cat;
      });
    usd = cat;
    return usd;
  }
  usd2sats(usd) {
    let sats;
    this.http
      .get('https://blockchain.info/ticker')
      .subscribe((data: { USD }) => {
        const price = data.USD.last;
        const usd3 = (sats = usd / price);
        const done = usd3 * (1 / 0.00000001);
        sats = done.toFixed(2);
      });
    return sats;
  }
}
