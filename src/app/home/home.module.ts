import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
// import { HTTP } from '@ionic-native/http/ngx';
// import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HomePage } from './home.page';
import { CardPage } from '../card/card.page';

import { HomePageRoutingModule } from './home-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
    // HttpClientModule,
  ],
  declarations: [HomePage, CardPage],
  providers: [Clipboard, InAppBrowser],
})
export class HomePageModule {}
