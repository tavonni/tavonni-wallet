import {
  Component,
  OnInit,
  NgZone,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { Platform, ModalController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { BehaviorSubject, Observable } from 'rxjs';
import { CardData } from 'src/app/models/card-data.model';
import { ApiService } from './../services/api.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import * as bitl from '../../assets/buidl.min.js';
import { map } from 'rxjs/operators';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import cardDataWbalance from './../../assets/cardDataWbalance.json';
import { SettingsPage } from '../settings/settings.page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomePage implements OnInit {
  // cid: string;
  cardData: any;
  cardData$: Observable<any>;
  // cardData$: Observable<CardData>;
  result$: Observable<any>;
  pubKeys$ = new BehaviorSubject<any>(undefined);
  pubKey: string;
  scanned: boolean;

  constructor(
    private http: HttpClient,
    private api: ApiService,
    private router: Router,
    public route: ActivatedRoute,
    private platform: Platform,
    public alertController: AlertController,
    private changeDetectorRef: ChangeDetectorRef,
    private storage: NativeStorage,
    public modal: ModalController,
    private zone: NgZone
  ) {}

  callback: any = {
    success: (result) => {
      const mockResult = cardDataWbalance;
      result = mockResult; // comment this to read from the card
      this.scanned = true;
      this.storage.setItem('cardData', result).then(
        () => console.log('Stored cardData!'),
        (error) => console.error('Error storing cardData', error)
      );
      // this.pubKey$.next(result.walletPublicKey);
      this.cardData = result;
      if (result.status === 'Empty') {
        this.presentAlert();
      } else {
        this.pubKey = result.walletPublicKey;
        this.getAddress(result.walletPublicKey);
        this.zone.run(() => {});
      }
      this.zone.run(() => {});
    },
    error(error) {
      console.log('error: ' + JSON.stringify(error));
    },
  };

  callbackInit: any = {
    success: (result) => {
      console.log(result);
      // this.cardData$.subscribe((d) => {
      //   console.log(d);
      //   if (d.status === 'Empty') {
      //     this.presentAlert();
      //   } else {
      //     this.getAddress(result.walletPublicKey);
      //   }
      // });
    },
    error(error) {
      console.log('Card init error: ' + JSON.stringify(error));
    },
  };

  ngOnInit() {
    // this.pubKey$.next('');
  }

  getAddress(key) {
    const addys = bitl.pubToAddress(key);
    this.pubKeys$.next({ default: addys.p2wpkh, legacy: addys.p2pkh });
    this.storage
      .setItem('addresses', { default: addys.p2wpkh, legacy: addys.p2pkh })
      .then(
        () => console.log('Stored addresses!'),
        (error) => console.error('Error storing addresses', error)
      );
    console.log(addys);
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Initialize Card?',
      message:
        'This card is not initialized. Would you like to initialize it now?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          },
        },
        {
          text: 'Yes',
          handler: () => {
            this.initCard();
          },
        },
      ],
    });

    await alert.present();
  }

  async settings() {
    const modal = await this.modal.create({
      component: SettingsPage,
    });
    return await modal.present();
  }

  initCard() {
    const cardId = this.cardData.cardId;
    (window as any).TangemSdk.createWallet(this.callbackInit, cardId);
  }

  scanCard() {
    (window as any).TangemSdk.scanCard(this.callback);
    // this.callback.success();
  }
}
