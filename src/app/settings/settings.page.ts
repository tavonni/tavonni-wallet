import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { NativeStorage } from '@ionic-native/native-storage/ngx';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  constructor(public modal: ModalController, private storage: NativeStorage) {}
  cardData: any;
  cid: string;
  ngOnInit() {
    this.storage.getItem('cardData').then(
      (data) => {
        this.cid = data.cardId;
      },
      (error) => console.error(error)
    );
  }
  close() {
    this.modal.dismiss();
  }
}
