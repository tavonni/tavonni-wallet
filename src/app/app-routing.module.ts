import { NgModule, Injectable } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () =>
      import('./home/home.module').then((m) => m.HomePageModule),
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: 'card/:addy',
    loadChildren: () =>
      import('./card/card.module').then((m) => m.CardPageModule),
  },
  {
    path: 'qr-modal',
    loadChildren: () =>
      import('./qr-modal/qr-modal.module').then((m) => m.QrModalPageModule),
  },
  {
    path: 'receive',
    loadChildren: () =>
      import('./receive/receive.module').then((m) => m.ReceivePageModule),
  },
  {
    path: 'send',
    loadChildren: () =>
      import('./send/send.module').then((m) => m.SendPageModule),
  },
  {
    path: 'settings',
    loadChildren: () => import('./settings/settings.module').then( m => m.SettingsPageModule)
  },
];
@Injectable({
  providedIn: 'root',
})
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
