import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';
import { NavController, NavParams } from '@ionic/angular';
import { CardPageRoutingModule } from './card-routing.module';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

import { CardPage } from './card.page';

@NgModule({
  imports: [CommonModule, IonicModule, CardPageRoutingModule],
  declarations: [CardPage],
  providers: [Clipboard, InAppBrowser],
})
export class CardPageModule {}
