import { Component, OnInit, Input } from '@angular/core';
import { Location, formatCurrency } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import {
  HttpClient,
  HttpClientModule,
  HttpHeaders,
} from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { ModalController } from '@ionic/angular';
import { QrModalPage } from '../qr-modal/qr-modal.page';
import { ReceivePage } from '../receive/receive.page';
import { SendPage } from '../send/send.page';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { ToastController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
// import opennode from 'opennode';
// import * as bitcoin from '../../assets/bitcoinjs.js';
// import * as bitl from '../../assets/buidl.min.js';
import * as _ from 'lodash';
import { ApiService } from './../services/api.service';
// import * as bech32 from 'bech32';
import { HomePage } from '../home/home.page';

// import { Converter } from './../services/converter';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-card',
  templateUrl: './card.page.html',
  styleUrls: ['./card.page.scss'],
})
export class CardPage implements OnInit {
  @Input() addys: any;
  @Input() pKey;
  apiPrice: any;
  btcPrice: any;
  btcPriceRaw: any;
  addy: any;
  txRefs: any;
  balanceBTC: any;
  balanceUSD: any;
  // balanceUSD$: Observable<any>;
  balanceBTC$ = new BehaviorSubject<any>(null);
  balanceUSD$ = new BehaviorSubject<any>(null);
  btcPrice$ = new BehaviorSubject<any>(null);

  constructor(
    private route: ActivatedRoute,
    private http: HttpClient,
    private router: Router,
    private clipboard: Clipboard,
    public modal: ModalController,
    public toast: ToastController,
    private browser: InAppBrowser,
    private api: ApiService,
    private home: HomePage,
    private location: Location // private convert: Converter
  ) {}

  ngOnInit() {
    this.getPrice();
    this.getWalletBalance();
  }

  getPrice() {
    // prettier-ignore
    this.http
      .get(
        'https://blockchain.info/ticker'
      )
      .subscribe((data: { USD}) => {
        this.btcPriceRaw = data.USD.last;
        // this.btcPrice = formatCurrency(data.USD.last, 'en-US', '$');
        this.btcPrice$.next(formatCurrency(data.USD.last, 'en-US', '$'));
      });
    // this.convert.getPrice().subscribe((res: {USD}) => {
    //   this.btcPriceRaw = res.USD.last;
    // });
  }

  getWalletBalance() {
    // prettier-ignore
    this.http.get(
        'https://api.blockcypher.com/v1/btc/main/addrs/' + this.addys.legacy + '')
      .subscribe((data: {balance, txRefs}) => {
        console.log(data);
        this.balanceBTC = this.satsToBtc(data.balance);
        this.balanceBTC$.next(this.satsToBtc(data.balance));
        this.balanceUSD = this.btcToUsd(data);
        this.txRefs = data.txRefs;
        // this.balanceUSD$.next(this.balanceUSD);
        // this.btcToUsd(this.balanceBTC);
        // return data.data.balance;
      });
    // const a = this.api.getAddressFromKey(this.addy);
    // console.log('here ' + a);
  }

  async showQRcode(addy) {
    const modal = await this.modal.create({
      component: QrModalPage,
      componentProps: {
        addy,
      },
    });
    return await modal.present();
  }

  async showReceive() {
    const modal = await this.modal.create({
      component: ReceivePage,
      componentProps: {
        addy: this.addy,
      },
    });
    return await modal.present();
  }
  async showSend(address) {
    const modal = await this.modal.create({
      component: SendPage,
      componentProps: {
        btc: this.balanceBTC,
        usd: this.balanceUSD,
        addy: address,
        btcPrice: this.btcPriceRaw,
      },
    });
    return await modal.present();
  }

  async copiedToast() {
    const toast = await this.toast.create({
      message: 'Your bitcoin address has been copied.',
      duration: 2000,
    });
    toast.present();
  }
  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modal.dismiss({
      dismissed: true,
    });
  }
  goBack() {
    //  this.router.navigate(['/']);
    this.location.back();
  }

  copyToClipboard(me) {
    this.clipboard.copy(me);
    this.copiedToast();
  }
  satsToBtc(sats) {
    const result = (sats * 0.00000001).toString();
    return result.substr(0, 10);
  }
  btcToSats(btc) {
    const result = (btc / 0.00000001).toString();
    return result.substr(0, 10);
  }
  satsToUsd(btc) {
    const result = (btc / 0.00000001).toString();
    return result.substr(0, 10);
  }
  btcToUsd(btc) {
    const btcb: number = +this.balanceBTC;
    const btcp: number = +this.btcPriceRaw;
    this.balanceUSD = formatCurrency(btcb * btcp, 'en-US', '$');
    this.balanceUSD$.next(this.balanceUSD);
  }

  exploreAddy(addy) {
    const browser = this.browser.create(
      'https://www.blockchain.com/btc/address/' + addy,
      '_system'
    );
    browser.show();
  }

  createCharge() {
    // Create a new charge
    // opennode.setCredentials('c8b9003e-9aee-4c37-bf10-4977ebf11fd8', 'live');
    // const charge = {
    //   amount: 2.0,
    //   currency: 'USD',
    //   callback_url: 'https://example.com/webhook/opennode',
    //   auto_settle: false,
    // };
    // // prettier-ignore
    // opennode.createCharge(charge)
    //   .then((response) => {
    //     console.log(response);
    //   })
    //   .catch((error) => {
    //     console.error(`${error.status} | ${error.message}`);
    //   });
  }

  // getChargeInfo() {
  //   // prettier-ignore
  //   opennode.chargeInfo('47bb5224-bf50-49d0-a317-4adfd345221a')
  //     .then((charge) => {
  //       console.log(charge);
  //     })
  //     .catch((error) => {
  //       console.error(`${error.status} | ${error.message}`);
  //     });
  // }

  scanCard() {
    this.home.scanCard();
  }
}

// open node api keys
// dev1-invoices: c8b9003e-9aee-4c37-bf10-4977ebf11fd8
// read only a1198f96-a3e8-467d-878d-cc63fc0edc84
// withdrawls fc128d12-5339-4690-9d13-2c13efa5d350
// e-comm key 41ab790a-cc4b-4290-a229-aa356531e97a
