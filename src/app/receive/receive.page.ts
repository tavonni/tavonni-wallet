import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ModalController } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-receive',
  templateUrl: './receive.page.html',
  styleUrls: ['./receive.page.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReceivePage implements OnInit {
  form: FormGroup;
  @Input() addy: string;
  tempAddy$ = new BehaviorSubject<any>(undefined);
  error$ = new BehaviorSubject<string>(undefined);

  constructor(
    private http: HttpClient,
    public modal: ModalController,
    private formBuilder: FormBuilder
  ) {
    // this.form = this.formBuilder.group({
    //   amount: ['', Validators.required],
    //   description: [''],
    // });
  }

  ngOnInit() {
    // this.getTempAddy();
  }

  // TODO: Implement a random forwarding address to recieve payments.
  //  This gets a temp forwarding address to send the payment to.
  getTempAddy() {
    const payment = {
      destination: this.addy,
    };
    this.http
      .post(
        'https://api.blockcypher.com/v1/btc/main/forwards?token=c33754efab41467cb340559028f84329',
        JSON.stringify(payment)
      )
      .subscribe({
        next: (data) => {
          console.log(data);
          this.tempAddy$.next(data);
        },
        error: (error) => {
          this.error$.next(error.error.error);
        },
      });
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modal.dismiss({
      dismissed: true,
    });
  }
}
