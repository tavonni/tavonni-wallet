import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

@Component({
  selector: 'app-qr-modal',
  templateUrl: './qr-modal.page.html',
  styleUrls: ['./qr-modal.page.scss'],
})
export class QrModalPage implements OnInit {
  @Input() addy: string;
  encodeData: any;

  constructor(
    public modal: ModalController,
    private barcodeScanner: BarcodeScanner
  ) {}

  ngOnInit() {}

  dismiss() {
    this.modal.dismiss({
      dismissed: true,
    });
  }
}
