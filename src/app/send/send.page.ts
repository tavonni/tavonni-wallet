import { Component, OnInit, Input, NgZone } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { FormControl } from '@angular/forms';
import { prefix, RxFormBuilder } from '@rxweb/reactive-form-validators';
import { SendModel } from './send.model';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, combineLatest } from 'rxjs';
import { ApiService } from './../services/api.service';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import * as bitcoin from '../../assets/bitcoinjs.js';
import * as conv from 'binstring';
// import { Converter } from './../services/converter';

@Component({
  selector: 'app-send',
  templateUrl: './send.page.html',
  styleUrls: ['./send.page.scss'],
})
export class SendPage implements OnInit {
  constructor(
    public modal: ModalController,
    private barcodeScanner: BarcodeScanner,
    private formBuilder: RxFormBuilder,
    private clipboard: Clipboard,
    private http: HttpClient,
    private zone: NgZone,
    private storage: NativeStorage,
    private api: ApiService // private convert: Converter
  ) {}
  @Input() btc: string;
  @Input() usd: string;
  @Input() addy: string;
  @Input() btcPrice: string;
  form = this.formBuilder.formGroup(SendModel);
  balanceUSD: any;
  cid: string;
  pubKey: string;
  txSigned: string;
  tx: any;
  // balanceUSD$: Observable<any>;
  balanceBTC$ = new BehaviorSubject<any>(null);
  balanceUSD$ = new BehaviorSubject<any>(null);
  fee$ = new BehaviorSubject<any>(null);
  totalTx$ = new BehaviorSubject<any>(null);
  error$ = new BehaviorSubject<any>(null);
  tosign$ = new BehaviorSubject<any>(null);
  signCallback: any = {
    success: (result) => {
      console.log(result);
      this.txSigned = result.signature;

      this.tx.signatures = [this.hexIt(result.signature)];
      this.tx.pubkeys = [this.hexIt(this.pubKey)];
      this.zone.run(() => {});
    },
    error(error) {
      console.log('error: ' + JSON.stringify(error));
    },
  };

  ngOnInit() {
    this.form.reset();
    this.storage.getItem('cardData').then(
      (data) => {
        this.cid = data.cardId;
        this.pubKey = data.walletPublicKey;
      },
      (error) => console.error(error)
    );
  }

  scanCode() {
    this.barcodeScanner
      .scan()
      .then((barcodeData) => {
        this.form.controls.toAddress.setValue(barcodeData.text);
      })
      .catch((err) => {
        this.error$.next(err);
        console.log('Error', err);
      });
  }

  pasteFromClipboard() {
    this.clipboard.paste().then(
      (resolve: string) => {
        this.form.controls.toAddress.setValue(resolve);
      },
      (reject: string) => {
        console.log('Error: ' + reject);
      }
    );
  }

  getMinerFee(pref?) {
    // For example, if the recommended fee is 110 satoshis/byte and the median byte size is 226 bytes, that's 226 * 110 = 24860
    //  https://bitcoiner.live/doc/api
    // https://www.blockcypher.com/dev/bitcoin/?javascript#customizing-transaction-requests
    pref = pref || 'medium';
    // const sats = this.usd2sats(this.form.controls.amount.value);
    // console.log(sats);
    const payload = {};
    const url = 'https://bitcoiner.live/api/fees/estimates/latest';

    this.http.post(url, JSON.stringify(payload)).subscribe(
      (data: { estimates: [] }) => {
        // this.fee$.next(data.estimates);
        // console.log(data.estimates.30.total.p2wpkh.usd);
      },
      (error) => {
        this.error$.next(error.error.errors);
      }
    );
  }
  // create a transaction and returns a txid
  // can pass in the pref to change the fee
  // can pass send=true to broadcast the transaction to the network.
  createTx(pref?, send?) {
    pref = pref || 'medium';
    send = send || false;
    const sendingSats = this.usd2sats(this.form.controls.amount.value);
    const payload = {
      inputs: [{ addresses: [this.addy] }],
      outputs: [
        {
          addresses: [this.form.controls.toAddress.value],
          value: parseInt(sendingSats),
        },
      ],
      preference: pref,
    };

    const url =
      'https://api.blockcypher.com/v1/btc/main/txs/new?includeToSignTx=true';
    this.http.post(url, payload).subscribe(
      (data: { tx; tosign }) => {
        console.log(data);
        this.tx = data;
        this.fee$.next(data.tx.fees);
        this.totalTx$.next(parseInt(sendingSats) + data.tx.fees);
        this.tosign$.next(data.tosign);
      },
      (error) => {
        console.log(error.error.errors);
        this.error$.next(error.error.errors);
        return error.error.errors;
      }
    );
  }

  decodeTx(txhex) {
    const payload = {
      tx: txhex,
    };

    const url = 'https://api.blockcypher.com/v1/bcy/test/txs/decode';
    this.http.post(url, JSON.stringify(payload)).subscribe(
      (data) => console.log(data),
      (error) => {
        console.log(error.error);
        return error.error;
      }
    );
  }

  sendTx() {
    console.log(this.tx);

    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
      }),
    };
    const url = 'https://api.blockcypher.com/v1/btc/main/txs/send';
    this.http.post(url, this.tx, headers).subscribe(
      (data) => console.log(data),
      (error) => {
        this.error$.next(error.error.errors);
        console.log(error.error.errors);
      }
    );
  }
  signTx() {
    let txhex;
    this.tosign$.subscribe((x) => {
      console.log(x);
      txhex = x;
    });
    (window as any).TangemSdk.sign(this.signCallback, this.cid, txhex);
  }
  // https://codepen.io/mikalv/pen/GELpvG?editors=1010  converters

  satsToUsd(btc) {
    // tslint:disable-next-line:one-variable-per-declaration
    let total, totalSats;
    totalSats = btc * 0.00000001;
    total = totalSats * parseInt(this.btcPrice);
    return total.toFixed(2);
  }
  satsToBtc(sats) {
    const result = (sats * 0.00000001).toString();
    return result.substr(0, 10);
  }
  btcToSats(sats) {
    const result = (sats / 0.00000001).toString();
    return result.substr(0, 10);
  }
  usd2sats(usd) {
    let sats;
    const usd3 = (sats = usd / parseInt(this.btcPrice));
    const done = usd3 * (1 / 0.00000001);
    sats = done.toFixed(0);
    return sats;
  }
  toHex(str) {
    return conv(str, { in: ' bytes', out: 'hex' });
  }
  hexIt(str) {
    console.log(conv(str, { out: 'hex' }));
    return conv(str, { out: 'hex' });
  }
  txtIt(str1) {
    const hex = str1.toString();
    let str = '';
    for (let n = 0; n < hex.length; n += 2) {
      str += String.fromCharCode(parseInt(hex.substr(n, 2), 16));
    }
    return str;
  }

  parseHex(str) {
    const result = [];
    while (str.length >= 8) {
      result.push(parseInt(str.substring(0, 8), 16));

      str = str.substring(8, str.length);
    }

    return result;
  }

  createHex(bytes) {
    const hex = [];
    let i = 0;
    for (i; i < bytes.length; i++) {
      const current = bytes[i] < 0 ? bytes[i] + 256 : bytes[i];
      hex.push((current >>> 4).toString(16));
      hex.push((current & 0xf).toString(16));
    }
    console.log(hex.join(''));
    return hex.join('');
  }

  dismiss() {
    this.modal.dismiss({
      dismissed: true,
    });
  }
}
