import { prop, required } from '@rxweb/reactive-form-validators';

export class SendModel {
  @prop()
  @required({ message: 'Address is required' })
  toAddress: string;

  @prop()
  @required({ message: 'Amount is required' })
  amount: string;

  @prop()
  description: string;
}
